// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
function comun() {
		$(function() {
			$( "#accordion" ).accordion();
		
			$( "#radioset" ).buttonset();


			// Hover states on the static widgets
			$( "#icons li" ).hover(
				function() {
					$( this ).addClass( "ui-state-hover" );
				},
				function() {
					$( this ).removeClass( "ui-state-hover" );
				});
		});
		$(document).ready(function() {		
			$('ul li:has(ul)').hover(
				function(e) {
					$(this).find('ul').css({display: "block"});
				},
				function(e) {
					$(this).find('ul').css({display: "none"});
				});
		});
}
