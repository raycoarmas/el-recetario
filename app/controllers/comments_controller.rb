class CommentsController < ApplicationController
  before_action :load_recipe
  before_action :authenticate
  def create
    @comment = @recipe.comments.new(comment_params)
    if @comment.save
      redirect_to @recipe, notice: 'Thanks for you comment'
    else
      redirect_to @recipe, alert: 'Unable to add comment'
    end
  end

private
  def load_recipe
    @recipe = Recipe.find_by_id(params[:recipe_id])
  end
  def comment_params
    params.require(:comment).permit(:name, :body)
  end
end
