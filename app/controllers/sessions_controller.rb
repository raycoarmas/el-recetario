class SessionsController < ApplicationController
  

  def create
    if user= User.authenticate(params[:usuario],params[:password])
      session[:user_id]=user.id
      redirect_to root_path, notice: 'Logged in succsessfully'
    else
      flash.now[:alert] = "invalid usuario/password combination"
      render :new    
    end
  end

  def destroy
    reset_session
    redirect_to root_path, notice: 'logged out successfully'
  end
end
