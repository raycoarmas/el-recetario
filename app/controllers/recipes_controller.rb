class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :edit, :update, :destroy]
  
  # GET /recipes
  # GET /recipes.json
  def index
    @recipes = Recipe.last(3)
    @recip= "seleccionado"
    
  end

  def filtrado
    @recipes = Recipe.search(params)
    respond_to do |format|
      format.js 
    end
    
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
     @recip= "seleccionado"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end
end
