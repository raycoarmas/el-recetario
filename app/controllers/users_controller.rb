class UsersController < ApplicationController
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)  
      if @user.save
        redirect_to root_path, notice: 'Usuario creado satisfactoriamente'
      else
        render action: :new
      end    
  end

  private
    def set_user
      @user = current_user
    end

    def user_params
      params.require(:user).permit(:usuario, :password)
    end
end
