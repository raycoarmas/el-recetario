class Recipe < ActiveRecord::Base

   #include ActionController::MimeResponds
   #scope :filtrado, lambda{|ingrediente| where("recipes.ingrediente LIKE ?", "%#{val}%")}

  has_many  :comments

  def self.search(val)
    if val[:ingrediente] != "" and val[:plato] != "" and val[:dificultad] != ""
      where("ingrediente = ? and plato = ? and dificultad = ?", "#{val[:ingrediente]}","#{val[:plato]}","#{val[:dificultad]}")
    elsif val[:ingrediente] != "" and val[:plato] != ""
      where("ingrediente = ? and plato = ?", "#{val[:ingrediente]}","#{val[:plato]}")
    elsif val[:ingrediente] != "" and val[:dificultad] != ""
      where("ingrediente = ? and dificultad = ?", "#{val[:ingrediente]}","#{val[:dificultad]}")
    elsif val[:plato] != "" and val[:dificultad] != ""
      where("plato = ? and dificultad = ?","#{val[:plato]}","#{val[:dificultad]}")
    elsif val[:ingrediente] != ""
      where("ingrediente = ?", "#{val[:ingrediente]}")
    elsif val[:plato] != ""
      where("plato = ?","#{val[:plato]}")
    elsif val[:dificultad] != ""
       where("dificultad = ?", "#{val[:dificultad]}")
    end
  end
end
