require 'digest'
class User < ActiveRecord::Base
  validates :usuario, format: {with: /[a-zA-Z]+[-0-9.+_*]/, message: "Formato de nombre de usuario no válido"}
  validates :password, length: {in: 6..18}, confirmation: true

  has_many  :comments
  
  before_save :encrypt_new_password

  def self.authenticate(usuario,pass)
    user = find_by_usuario(usuario)
    return user if user && user.authenticated?(pass)
  end

  def authenticated?(pass)
    self.password == encrypt(pass)
  end

  protected
    def encrypt_new_password
      return if password.blank?
      self.password = encrypt(password)
    end
    
    def encrypt(string)
      Digest::SHA1.hexdigest(string)
    end 
end
