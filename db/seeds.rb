# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



Recipe.create(imagen: "verduras.jpg", titulo: "Empanadillas de Verduras", datos: "Verduras, postre, dificultad media", descripcion: "ffffffffffffffff fffffffffffffffffffffffffffffffffffff fffffffffffffffffffffffffffffffffffffffff ffffffffffffffffffffffffffffffffffffffffff fffffffff fffffff ffffffffffffffffff fffffffffffffffffff fffffffffffffffffffffffff ffffffffffffffffffffffffffffffff ffffffffffffffff fffffffffffffff", ingredientes: "tomate, pepino, calabaza, pollo", ingrediente: "verduras", dificultad: "facil", plato: "principal", video: "puppy.webm")

Recipe.create(imagen: "tarta.jpg", titulo: "Tarta 3 chocolates", datos: "Chocolates, postre, dificultad media", descripcion: "ffffffffffffffff fffffffffffffffffffffffffffffffffffff fffffffffffffffffffffffffffffffffffffffff ffffffffffffffffffffffffffffffffffffffffff fffffffff fffffff ffffffffffffffffff fffffffffffffffffff fffffffffffffffffffffffff ffffffffffffffffffffffffffffffff ffffffffffffffff fffffffffffffff", ingredientes: "tomate, pepino, calabaza, pollo", ingrediente: "chocolate", dificultad: "alta", plato: "postre", video: "puppy.webm")

Competition.create(imagen: "concurso1.png", comunidad: "canarias")
Competition.create(imagen: "concurso2.png", comunidad: "madrid")
Competition.create(imagen: "concurso3.png", comunidad: "canarias")

Trick.create(imagen: "descongelar.jpg", titulo: "Cómo descongelar alimentos", informacion: "Si quieres mantener la calidad de los alimentos debes descongelarlos ...")
Trick.create(imagen: "jordi.jpg", titulo: "Jordi Butrón, premio al mejor pastelero de España", informacion: "El foco de inspiración mundial está puesto en la cocina española y en sus postres ...")
Trick.create(imagen: "bolleria.jpg", titulo: "Información nutricional de la bollería", informacion: "Tabla con la información nutricional de la bollería")
Trick.create(imagen: "sal_pasta.jpg", titulo: "Cómo recordar si se ha puesto sal en el agua para cocer la pasta", informacion: "Para recorar si hemos echado sal al agua en la que vamos a cocer la pasta...")
Trick.create(imagen: "mariscos.jpg", titulo: "Información nutricional de los mariscos y los crustáceos", informacion: "Tabla con la información nutricional de los mariscos y los crustáceos")
Trick.create(imagen: "francisco.jpg", titulo: "Francisco Vicente, elegido mejor cocinero de España en el Salón de Gorumets", informacion: "El andaluz Francisco Vicente ha sido elegido mejor cocinero de España en el ...")
