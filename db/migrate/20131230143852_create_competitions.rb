class CreateCompetitions < ActiveRecord::Migration
  def change
    create_table :competitions do |t|
      t.string :imagen
      t.string :comunidad

      t.timestamps
    end
  end
end
