class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :imagen
      t.string :titulo
      t.text :datos
      t.text  :descripcion
      t.text  :ingredientes
      t.string :ingrediente
      t.string :dificultad
      t.string :plato
      t.string  :video

      t.timestamps
    end
  end
end
