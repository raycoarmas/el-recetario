class CreateTricks < ActiveRecord::Migration
  def change
    create_table :tricks do |t|
      t.string :imagen
      t.string :titulo
      t.text :informacion
      t.string :filtrado

      t.timestamps
    end
  end
end
